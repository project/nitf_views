<?php
<<<<<<< nitf_views_views_plugin_row_xml.inc
=======
>>>>>>> 1.2

/*
 * Created by Greg Harvey on May 15, 2009
 *
 * http://www.drupaler.co.uk
 */

/**
 * Plugin which performs a node_view on the resulting object
 * and formats it as an NITF XML document.
 */
class nitf_views_views_plugin_row_xml extends views_plugin_row {
  function option_definition() {
    $options = parent::option_definition();

    $options['nitf_version'] = array('default' => 'default');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['nitf_version'] = array(
      '#type' => 'select',
      '#title' => t('NITF Version'),
      '#options' => array(
        // make NITF version an option for future extension
        '3.4' => t('3.4 (latest)'),
      ),
      '#default_value' => $this->options['nitf_version'],
    );
    $form['nitf_publisher'] = array(
      '#type' => 'textfield',
      '#title' => t('Unique publisher string'),
      '#default_value' => $this->options['nitf_publisher'],
      '#required' => TRUE,
    );
    $form['nitf_publisher_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('Publisher domain (e.g. foo.com)'),
      '#default_value' => $this->options['nitf_publisher_domain'],
    );
    $form['nitf_owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Content copyright holder (if applicable)'),
      '#default_value' => $this->options['nitf_owner'],
    );
    $form['nitf_ftp'] = array(
      '#type' => 'textfield',
      '#title' => t('FTP server for media (if applicable)'),
      '#default_value' => $this->options['nitf_ftp'],
    );
    $form['nitf_media_embed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Embed media as base64 encoded string(s)'),
      '#default_value' => $this->options['nitf_media_embed'],
      '#description' => t('WARNING: enabling this option will cause memory issues if you have many or large files.'),
    );
    $form['nitf_media_chunks'] = array(
      '#type' => 'textfield',
      '#title' => t('Media chunk size (if embedding media as base64)'),
      '#default_value' => $this->options['nitf_media_chunks'] ? $this->options['nitf_media_chunks'] : 0,
      '#description' => t('If you intend to base64 encode your external media files, this setting allows you to optionally break those files in to chunks. Set in bytes, set to 0 to disable and not chunk media at all but send it all at once.'),
    );
  }

  function render($row) {
    global $base_url;
    
    $publisher = $this->options['nitf_publisher'];
    $publisher_domain = $this->options['nitf_publisher_domain'];
    $owner = $this->options['nitf_owner'];
    $ftp = $this->options['nitf_ftp'];
    $chunks = $this->options['nitf_media_chunks'];
    $embed = $this->options['nitf_media_embed'];

    // Load the specified node and buld its content.
    $node = node_load($row->nid);
    $node->build_mode = 'nitf';

    // Filter and prepare node teaser
    if (node_hook($node, 'view')) {
      $node = node_invoke($node, 'view');
    }
    else {
      $node = node_prepare($node, FALSE);
    }

    // Allow modules to change $node->teaser before viewing.
    node_invoke_nodeapi($node, 'view');

    // Set the proper node part, then unset unused $node part so that a bad
    // theme can not open a security hole.
    $content = drupal_render($node->content);
    $node->body = $content;
    unset($node->teaser);
  
    // Allow modules to modify the fully-built node.
    node_invoke_nodeapi($node, 'alter');

    // Now we start populating the NITF XML entry itself.
    $entry = new stdClass();
    $entry->title = $node->title;
    $entry->id = "tag:$base_url,node-$node->nid";
    $entry->updated = format_date($node->changed, 'custom', 'Ymd\THisO');
    $entry->publisher = $publisher;
    $entry->publisher_domain = $publisher_domain;
    $entry->owner = $owner;
    $entry->ftp = $ftp;
    $entry->chunk_size = $chunks;
    $entry->embed = $embed;

    // Might as well stick the originally-published date in there while we're at it.
    $entry->elements['published'] = format_date($node->created);

    // Prepare the 'author' chunk.
    /*
     if ($author = atom_views_format_author($node)) {
      $entry->elements[] = array(
        'key' => 'author',
        'value' => $author,
      );
    }
     */

    // Always add a basic link back to the item itself.
    $entry->elements['link'] = array(
      'href' => url('node/' . $node->nid, array('absolute' => TRUE)),
    );

    $entry->node = $node;

    //drupal_alter('atom_entry', $entry, 'node', $node);
    return theme($this->theme_functions(), $this->view, $this->options, $entry);
  }
}
