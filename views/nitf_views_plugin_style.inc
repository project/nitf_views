<?php
<<<<<<< nitf_views_plugin_style.inc
=======
>>>>>>> 1.2

/**
 * @file
 * Contains the Atom style plugin.
 */

/**
 * Default style plugin to render an Atom feed.
 *
 * @ingroup views_style_plugins
 */
class atom_views_plugin_style extends views_plugin_style {
  function attach_to($display_id, $path, $title) {
    $display = $this->view->display[$display_id]->handler;
    $url_options = array();
    $input = $this->view->get_exposed_input();
    if ($input) {
      $url_options['query'] = $input;
    }

    $url = url($this->view->get_url(NULL, $path), $url_options);
    if ($display->has_path()) {
      if (empty($this->preview)) {
        drupal_add_feed($url, $title);
      }
    }
    else {
      if (empty($this->view->feed_icon)) {
        $this->view->feed_icon = '';
      }

      $this->view->feed_icon .= theme('feed_icon', $url, $title);
      drupal_add_link(array(
        'rel' => 'alternate',
        'type' => 'application/atom+xml',
        'title' => $title,
        'href' => $url
      ));
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['slogan'] = array('default' => '', 'translatable' => TRUE);
    $options['use_site_slogan'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['use_site_slogan'] = array(
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['use_site_slogan']),
      '#title' => t('Use the site slogan for the feed subtitle'),
    );
    $form['subtitle'] = array(
      '#type' => 'textfield',
      '#title' => t('Feed slogan'),
      '#default_value' => $this->options['subtitle'],
      '#description' => t('This will appear in the Atom feed itself.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-style-options-use-site-slogan' => array(FALSE)),
    );
  }

  /**
   * Return an array of additional XHTML elements to add to the channel.
   *
   * @return
   *   An array that can be passed to format_xml_elements().
   */
  function get_channel_elements() {
    $view = $this->view;

    $elements = array();

    // If there's a display this feed is attached to, add it as an alternate link
    $link_display_id = $view->display_handler->get_link_display();
    if ($link_display_id && !empty($view->display[$link_display_id])) {
      $path = $view->display[$link_display_id]->handler->get_path();
      if ($path) {
        $path = $view->get_url(NULL, $path);
        // Compare the link to the default home page; if it's the default home page, just use $base_url.
        if ($path == variable_get('site_frontpage', 'node')) {
          $path = '';
        }
    
        $elements[] = array(
          'key' => 'link',
          'attributes' => array(
            'rel' => 'alternate',
            'href' => url($path),
          ),
        );
      }
    }

    // Feed subtitle
    if (!empty($this->options['use_site_slogan'])) {
      $subtitle = variable_get('site_slogan', '');
    }
    else {
      $subtitle = $this->options['subtitle'];
    }
    if (!empty($subtitle)) {
      $elements['subtitle'] = decode_entities(filter_xss($subtitle));
    }
  
    return $elements;
  }

  function render() {
    if (empty($this->row_plugin)) {
      vpr('views_plugin_style_default: Missing row plugin');
      return;
    }
    $rows = array();

    $this->feed_elements = $this->get_channel_elements();
    foreach ($this->view->result as $row) {
      $rows[] = $this->row_plugin->render($row);
    }
    $this->entries = $rows;

    // Toss up a generic alter hook so people have an opportunity to alter the
    // view itself, and the individual rows.
    drupal_alter('atom_feed', $this);
    
    $rows = $this->entries;

    return theme($this->theme_functions(), $this->view, $this->options, $rows);
  }
}
