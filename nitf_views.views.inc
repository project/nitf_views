<?php
<<<<<<< nitf_views.views.inc
=======
>>>>>>> 1.2

/*
 * Created by Greg Harvey on May 15, 2009
 *
 * http://www.drupaler.co.uk
 */


/**
 * Implementation of hook_views_plugins().
 */
function nitf_views_views_plugins() {
  return array(
    //'module' => 'nitf_views',
    'row' => array(
      'node_nitf' => array(
        'title' => t('Node (NITF XML format)'),
        'help' => t('Display nodes as NITF XML documents.'),
        'handler' => 'nitf_views_views_plugin_row_xml',
        'path' => drupal_get_path('module', 'nitf_views') . '/views',
        'theme' => 'nitf_views_row',
        'theme path' => drupal_get_path('module', 'nitf_views'),
        'uses options' => TRUE,
        'type' => 'feed',
        'help topic' => 'style-nitf',
      ),
    ),
  );
}

/**
 * Template preprocess for adding variables to the row template
 * for this row style.
 */
function template_preprocess_nitf_views_row(&$vars) {
  //print_r($vars);
  $vars['node'] = $vars['row']->node;
  $entry = &$vars['row'];
  
  // add a link to the original node
  $vars['link'] = check_url($entry->elements['link']['href']);
  
  // if document is revised get the new VID to pass to template
  if (count(node_revision_list($vars['node'])) > 1) {
    // note, we want the SECOND row, hence query range only returning that
    $query = db_query_range("SELECT vid FROM {node_revisions} WHERE nid = %d ORDER BY vid DESC", $vars['node']->nid, 1, 1);
    $row = db_fetch_array($query);
    $vars['pvid'] = $row['vid'];
    $vars['sent'] = format_date($vars['node']->revision_timestamp, 'custom', 'Ymd\THisO');
    $vars['revision_list'] = node_revision_list($vars['node']);
  }
  
  // TODO:
  // if the Location module exists then create a $location array for evloc data
  if (module_exists('location')) {
    $location = array();
    $vars['location'] = $location;
  }
  
  if ($vars['node']->field_story_keywords[0]['value']) {
    $keywords = split(',', $vars['node']->field_story_keywords[0]['value']);
    $keywords = array_map('trim', $keywords);
    $vars['keywords'] = $keywords;
  }
  
  if ($vars['node']->taxonomy) {
    $vars['top_term'] = current($vars['node']->taxonomy);
  }
  
  // for each supported content type we'll need to say which field is the subhead (if any)
  switch ($vars['node']->type) {
    case 'story':
      $vars['subhead'] = $vars['node']->field_story_subhead[0]['value'];
      break;
  }
  
  // include the body wordcount as a variable
  $body = $vars['node']->content['body'];
  $vars['wordcount'] = count(explode(" ", strip_tags(trim($body['#value']))));
  
  // set empty array ready for media data
  $vars['media'] = array();
  
  // set chunk size - if it is 0 then chunking is off
  $chunk_size = $entry->chunk_size;
  
  // FTP base URL, if required
  if ($entry->ftp) {
    $ftp = 'ftp://' . $entry->ftp;
  }
  
  
  // FIRSTLY SET UP OUR TMG CROPPED THUMBNAIL AND MAIN IMAGES
  
  // check we have a file
  if ($vars['node']->field_story_image_crop[0]['filepath']) {
  
    // state where files of this type are for FTP download,
    // relative to $ftp, if required
    $ftp_path = 'imagecache/teaser/images/article';
    
    // load path to teaser imagecache preset
    $filepath = imagecache_create_path('teaser', $vars['node']->field_story_image_crop[0]['filepath']);
    
    // check the teaser imagecache preset has been generated
    if (!file_exists($filepath)) {
      // if not, build it now
      $path = $vars['node']->field_story_image_crop[0]['filepath'];
      $preset = imagecache_preset_by_name('teaser');
      $dst = imagecache_create_path($preset['presetname'], $path);
      imagecache_build_derivative($preset['actions'], $path, $dst);
    }
  
    // get the dimensions of the teaser image
    $idims = getimagesize($filepath);  
    $idims['width'] = $idims[0];
    $idims['height'] = $idims[1];
  
    $field = $vars['node']->field_story_image_crop;
  
    $vars['media']['crop_thumb'] = _nitf_views_build_media('image', $vars['node'], $field, $idims, $filepath, FALSE, $ftp, $ftp_path);
    
    // state where files of this type are for FTP download,
    // relative to $ftp, if required
    $ftp_path = 'imagecache/article/images/article';
    
    // load path to article (main) imagecache preset
    $filepath = imagecache_create_path('article', $vars['node']->field_story_image_crop[0]['filepath']);
    
    // check the article (main) imagecache preset has been generated
    if (!file_exists($filepath)) {
      // if not, build it now
      $path = $vars['node']->field_story_image_crop[0]['filepath'];
      $preset = imagecache_preset_by_name('article');
      $dst = imagecache_create_path($preset['presetname'], $path);
      imagecache_build_derivative($preset['actions'], $path, $dst);
    }
  
    // get the dimensions of the article (main) image
    $idims = getimagesize($filepath);  
    $idims['width'] = $idims[0];
    $idims['height'] = $idims[1];
    
    $vars['media']['crop_main'] = _nitf_views_build_media('image', $vars['node'], $field, $idims, $filepath, FALSE, $ftp, $ftp_path);
  
  }
  
  // THEN PROCESS ANY OTHER ATTACHED MEDIA
  
  // prepare media data for images
  // TODO: CCK mapping for image content - expects a node reference
  foreach ($vars['node']->field_story_images as $image) {
    // valid NITF media-type
    $media_type = 'image';
    
    // enable embedded media for this type
    // CAUTION: enabling this when media files are very large *will*
    // cause PHP memory problems
    $embed_media = $entry->embed;
    
    // state where files of this type are for FTP download,
    // relative to $ftp, if required
    $ftp_path = 'articles/photos';
    
    // make sure we have an image
    if ($image['nid'] && !$vars['media'][$media_type][$image['nid']]) {
      
      // load the actual image node
      $inode = node_load($image['nid']);
      
      $field = $inode->field_photo;
      
      // get the dimensions of the image
      $idims = getimagesize($inode->field_photo[0]['filepath']);  
      $idims['width'] = $idims[0];
      $idims['height'] = $idims[1];
      
      // get the filepath ready for use
      $filepath = file_create_path($inode->field_photo[0]['filepath']);
      
      // if the file is too big and we're intending to embed media, chunk it
      if (filesize($filepath) > $chunk_size && $chunk_size > 0 && $embed_media) {
        // open file and split in to chunks
        $data = chunk_split(base64_encode(fread(fopen($filepath, "rb"), filesize($filepath))), $chunk_size);
        $chunks = explode("\r\n", $data);
        unset($data); // clear memory space
        
        // set up loop counter
        $total = count($chunks);
        $count = 0;
        
        // loop through array and create media element for each chunk
        while ($count < $total) {            
          $vars['media'][$media_type][$vnode->nid] = _nitf_views_build_media($media_type, $inode, $vnode->$field, $idims, $filepath, $embed_media, $ftp, $ftp_path, $count, $chunks[$count]);
          $count++;            
        }
      } else {
        // populate the $media array
        $vars['media'][$media_type][$inode->nid] = _nitf_views_build_media($media_type, $inode, $field, $idims, $filepath, $embed_media, $ftp, $ftp_path);
      }
    }
  }
  
  // prepare media data for videos
  // currently assumes you are using FlashVideo with CCK filefield support
  // use FlashVideo 6.x1.5-rc3 and above (or 6.x-1.x-dev prior to rc3 release)
  if (module_exists('flashvideo') && module_exists('flashvideo_cck')) {
    // TODO: CCK mapping for video content field name 
    // expects a node reference
    foreach ($vars['node']->field_story_videos as $video) {
      // valid NITF media-type
      $media_type = 'video';
      
      // enable embedded media for this type
      // CAUTION: enabling this when media files are very large *will*
      // cause PHP memory problems
      $embed_media = $entry->embed;
      
      // state where files of this type are for FTP download,
      // relative to $ftp, if required
      $ftp_path = 'videos';
      
      if ($video['nid'] && !$vars['media'][$media_type][$video['nid']]) {
        
        // load the actual video node referred to
        $vnode = node_load($video['nid']);
        
        // get video dimensions from FlashVideo module
        $vdims = flashvideo_get_size($vnode);
        
        // retrieve the CCK file field containing the original video
        $vfield = flashvideo_variable_get($vnode->type, 'cck_original_video_field', 0);
        
        // quickly verify the field actually exists before using it
        if ($vfield) {
          $varray = $vnode->$vfield;
          
          // get the filepath ready for use
          $filepath = file_create_path($varray[0]['filepath']);
        }
        
        // verify the field exists again
        if ($vfield) {
        
          // if the file is too big and we're intending to embed media, chunk it
          if (filesize($filepath) > $chunk_size && $chunk_size > 0 && $embed_media) {
            // open file and split in to chunks
            $data = chunk_split(base64_encode(fread(fopen($filepath, "rb"), filesize($filepath))), $chunk_size);
            $chunks = explode("\r\n", $data);
            unset($data); // clear memory space
          
            // set up loop counter
            $total = count($chunks);
            $count = 0;
          
            // loop through array and create media element for each chunk
            while ($count < $total) {            
              $vars['media'][$media_type][$vnode->nid] = _nitf_views_build_media($media_type, $vnode, $vnode->$vfield, $vdims, $filepath, $embed_media, $ftp, $ftp_path, $count, $chunks[$count]);
              $count++;            
            }
          
          }
          else {      
            // populate the $media array
            $vars['media'][$media_type][$vnode->nid] = _nitf_views_build_media($media_type, $vnode, $vnode->$vfield, $vdims, $filepath, $embed_media, $ftp, $ftp_path);
          }
        }
      }
    }
  }
  
}

/**
 * Private function for populating the $media array used by
 * the template to render an NITF <media> element
 * 
 * @param $media_type
 *  string: must be a valid NITF media-type parameter
 *  http://www.iptc.org/std/NITF/3.4/documentation/nitf-documentation.html#media
 * 
 * @param $node
 *  object: the node holding the image file
 * 
 * @param $field
 *  array: the actual file field array from the node
 * 
 * @param $dims
 *  array: width and height of media
 * 
 * @param $filepath
 *  string: path to file on system, if embedding media
 * 
 * @param $embed_media
 *  boolean: flag to tell us if we need to base64 encode files
 * 
 * @param $ftp
 *  string: base FTP URL, if required
 * 
 * @param $ftp_path
 *  string: path from $ftp to the actual downloadable file
 * 
 * @param $chunk_id
 *  integer: the id for a chunk of a file, where 0 means no chunks
 * 
 * @param $chunk
 *  string: actual chunk of base64 encoded data
 * 
 * @return
 *  array: to be added in to the $media variable  
 */
function _nitf_views_build_media($media_type, $node, $field, $dims, $filepath = NULL, $embed_media = FALSE, $ftp = NULL, $ftp_path = NULL, $chunk_id = 0, $chunk = NULL) {
  $media = array();
  
  $media['mime'] = $field[0]['filemime'];
  $media['width'] = $dims['width'];
  $media['height'] = $dims['height'];
  
  if ($ftp && $ftp_path) {
    $media['source'] = $ftp . '/' . $ftp_path . '/' . $field[0]['filename'];
  }
  elseif ($ftp && !$ftp_path) {
    $media['source'] = $ftp . '/' . $field[0]['filename'];
  }
  else {
    $media['source'] = $field[0]['filename'];
  }
  
  if ($media_type == 'image' && $field[0]['data']['description']) {
    $media['caption'] = $field[0]['data']['description'];
  }
  elseif ($media_type == 'video' && $field[0]['data']['description']) {
    $media['caption'] = $field[0]['data']['description'];
  }
  
  if ($media_type == 'image') {
    $media['producer'] = $node->field_image_owner[0]['value'];
  }
  elseif ($media_type == 'video') {
    $media['producer'] = $node->field_video_owner[0]['value'];
  }
  
  if ($media_type == 'image' && $field[0]['data']['alt']) {
    $media['alt'] = $field[0]['data']['alt'];
  }
  
  // do we need to embed media files?
  if ($embed_media) {
    if ($chunk_id > 0 && $chunk) {
      $media['chunk'] = $chunk_id;
      // send actual binary file data for <media-object>
      $media['file'] = $chunk;
    }
    else {
      // send actual binary file data for <media-object>
      $binaryfile = fopen($filepath, 'rb');
      $media['file'] = base64_encode(fread($binaryfile, filesize($filepath)));
      fclose($binaryfile);
    }
  }
  
  return $media;
}
