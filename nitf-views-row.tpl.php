<?php
<<<<<<< nitf-views-row.tpl.php
=======
>>>>>>> 1.2

/*
 * Created by Greg Harvey on May 15, 2009
 *
 * http://www.drupaler.co.uk
 */

// if you need to change the standard date format output
// just change this variable:
$dateformat = 'Ymd\THisO';
?>

    <nitf uno="<?php print $row->publisher; ?>.<?php print $node->nid; ?>.<?php print $node->vid; ?>">
      <head>
        <title><?php print $node->title; ?></title>
        
        <tobject tobject.type="news"/>
<?php if ($revision_list): ?>
        <docdata management-status="usable" management-doc-idref="<?php print $row->publisher; ?>.<?php print $node->nid; ?>.<?php print $pvid; ?>" management-idref-status="usable">
<?php else: ?>
        <docdata management-status="usable" management-doc-idref="<?php print $row->publisher; ?>.<?php print $node->nid; ?>.0" management-idref-status="usable">
<?php endif; ?>
          <doc-id id-string="<?php print $row->publisher; ?>.<?php print $node->nid; ?>.<?php print $node->vid; ?>"/>
        
          <?php if ($location): ?><evloc city="<?php print $location['city']; ?>" state-prov="<?php print $location['province']; ?>" iso-cc="<?php print $location['country']; ?>"/><?php endif; ?>

          <?php if ($row->publisher_domain): ?><del-list>
            <from-src src-name="<?php print $row->publisher_domain; ?>" level-number="1"/>
          </del-list><?php endif; ?>
          
          
          <?php if ($top_term->name): ?><fixture fix-id="<?php print $top_term->name; ?>"/>
          <series series.name="<?php print $top_term->name; ?>" series.part="<?php print $node->nid; ?>" series.totalpart="0"/><? endif; ?>
        
          <?php if ($sent): ?><date.issue norm="<?php print $sent; ?>"/><?php endif; ?>
        
          <doc.copyright year="<?php print format_date($node->created, 'custom', 'Y'); ?>"<?php if ($row->owner): ?> holder="<?php print $row->owner; ?>"<?php endif; ?>/>
          <?php if ($row->owner): ?><doc.rights owner="<?php print $row->owner; ?>"/><?php endif; ?>
        
          <?php if ($keywords || $node->taxonomy): ?><key-list>
          <?php if ($keywords): foreach($keywords as $word) { ?>
  <keyword key="<?php print $word; ?>"/>
          <?php } endif; ?><?php if ($node->taxonomy): foreach($node->taxonomy as $term) { ?>
  <keyword key="<?php print $term->name; ?>"/>
          <?php } endif; ?></key-list><?php endif; ?>
        
        </docdata>
        
        <pubdata 
          type="web"<?php if ($row->publisher_domain): ?> 
          name="<?php print $row->publisher_domain; ?>" 
<?php endif; ?>
          date.publication="<?php print $pubdate = ($sent) ? $sent : format_date($node->created, 'custom', $dateformat); ?>" 
          unit-of-measure="character" 
          item-length="<?php print $wordcount; ?>" 
          number="<?php print $node->nid; ?>" 
          ex-ref="<?php print $link; ?>"
        />
        
        <?php if ($revision_list): foreach($revision_list as $revision) { ?>
<revision-history name="<?php print $revision->name; ?>" norm="<?php print format_date($revision->timestamp, 'custom', $dateformat); ?>"<?php if ($revision->log): ?> comment="<?php print $revision->log; ?>"<?php endif; ?>/>
        <?php } endif; ?>
        
      </head>
      
      <body>
        <body.head>
          <hedline>
            <hl1><?php print $node->title; ?></hl1>
            <?php if ($subhead): ?><hl2><?php print $subhead; ?></hl2><?php endif; ?>

          </hedline>
          
          <byline>
          By
            <person><?php print $node->field_story_byline[0]['value']; ?></person>
            <?php if ($row->owner): ?><byttl><?php print $row->owner; ?></byttl><?php endif; ?>

          </byline>
          
          <dateline>
            <story.date><?php print $pubdate = ($sent) ? $sent : format_date($node->created, 'custom', $dateformat); ?></story.date>
          </dateline>
        </body.head>
        
        <body.content>
        
        </body.content>
        
          <?php print $node->content['body']['#value']; ?>
          
<?php if ($media['crop_thumb']): ?>
          <media media-type="image">
            <media-reference 
              webpublish="Yes" 
              units="pixels" 
              id="drivingforce.teaser.<?php print $node->nid; ?>" 
              mime-type="<?php print $media['crop_thumb']['mime']; ?>" 
              source="<?php print $media['crop_thumb']['source']; ?>" 
              data-location="<?php print $media['crop_thumb']['source']; ?>" 
              height="<?php print $media['crop_thumb']['height']; ?>" 
              width="<?php print $media['crop_thumb']['width']; ?>"  
              copyright="Driving Force" 
              style="teaser" 
<?php if ($media['crop_thumb']['caption']): ?>              name="<?php print $media['crop_thumb']['caption']; ?>" 
<?php endif; ?>
<?php if ($media['crop_thumb']['alt']): ?>              alt="<?php print $media['crop_thumb']['alt']; ?>" <?php endif; ?> 
            />
<?php if ($media['crop_thumb']['caption']): ?>            <media-caption><?php print $media['crop_thumb']['caption']; ?></media-caption><?php endif; ?>

          </media>
<?php endif; ?>

<?php if ($media['crop_main']): ?>
          <media media-type="image">
            <media-reference 
              webpublish="Yes" 
              units="pixels" 
              id="drivingforce.main.<?php print $node->nid; ?>" 
              mime-type="<?php print $media['crop_main']['mime']; ?>" 
              source="<?php print $media['crop_main']['source']; ?>" 
              data-location="<?php print $media['crop_main']['source']; ?>" 
              height="<?php print $media['crop_main']['height']; ?>" 
              width="<?php print $media['crop_main']['width']; ?>"  
              copyright="Driving Force" 
              style="main" 
<?php if ($media['crop_main']['caption']): ?>              name="<?php print $media['crop_main']['caption']; ?>" 
<?php endif; ?>
<?php if ($media['crop_main']['alt']): ?>              alt="<?php print $media['crop_main']['alt']; ?>" <?php endif; ?> 
            />
<?php if ($media['crop_main']['caption']): ?>            <media-caption><?php print $media['crop_main']['caption']; ?></media-caption><?php endif; ?>

          </media>
<?php endif; ?>

<?php if ($media['image']): foreach($media['image'] as $nid => $image) { ?>
          <media media-type="image">
            <media-reference 
              units="pixels" 
              id="drivingforce.image.<?php print $nid; ?>" 
              mime-type="<?php print $image['mime']; ?>" 
              source="<?php print $image['source']; ?>" 
              data-location="<?php print $image['source']; ?>" 
              height="<?php print $image['height']; ?>" 
              width="<?php print $image['width']; ?>" 
              copyright="Driving Force" 
<?php if ($image['caption']): ?>              name="<?php print $image['caption']; ?>"  
<?php endif; ?>
<?php if ($image['alt']): ?>              alt="<?php print $image['alt']; ?>" 
<?php endif; ?>
            />
<?php if ($image['caption']): ?>            <media-caption><?php print $image['caption']; ?></media-caption><?php endif; ?>
            
            <media-producer><?php print $image['producer']; ?></media-producer>
<?php if ($image['file']): ?>            <media-object encoding="base64"<?php if ($image['chunk']): ?> id="node-<?php print $nid ?>-chunk-<?php print $image['chunk']; ?>"<?php endif; ?>><?php print $image['file']; ?></media-object><?php endif; ?>
            
          </media>
<?php } endif; ?>

<?php if ($media['video']): foreach($media['video'] as $nid => $video) { ?>
          <media media-type="video">
            <media-reference 
              id="drivingforce.video.<?php print $nid; ?>" 
              mime-type="<?php print $video['mime']; ?>" 
              source="<?php print $video['source']; ?>" 
              data-location="<?php print $video['source']; ?>" 
              height="<?php print $video['height']; ?>" 
              width="<?php print $video['width']; ?>"
              copyright="Driving Force" 
<?php if ($image['caption']): ?>              name="<?php print $video['caption']; ?>"  
<?php endif; ?>
            />
<?php if ($video['caption']): ?>            <media-caption><?php print $video['caption']; ?></media-caption><?php endif; ?>
            
            <media-producer><?php print $video['producer']; ?></media-producer>
<?php if ($video['file']): ?>            <media-object encoding="base64"<?php if ($video['chunk']): ?> id="node-<?php print $nid ?>-chunk-<?php print $video['chunk']; ?>"<?php endif; ?>><?php print $video['file']; ?></media-object><?php endif; ?>
            
          </media>
<?php } endif; ?>          
          
        <body.end/>
      
      </body>

    </nitf>
